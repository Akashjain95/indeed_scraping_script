import unittest
import time
import re
import sys
import os
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException 
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

url="https://www.indeed.com/resumes?q=fashion+designer&l="
driver =webdriver.PhantomJS()
f=open("scraped_data_fashiondesigner.txt", "a+")
while True: 
	global url
	driver.get(url)
	html_list =driver.find_element_by_id("results")
	items = html_list.find_elements_by_tag_name("li")
	for item in items:
		element=item.find_element_by_xpath(".//div[@class='sre-entry']/div[@class='sre-content']/div[@class='app_name']/a") 
		href=element.get_attribute('href')
		print href
		f.write(href+'\n')
	try:
		global url
        	element =driver.find_element_by_id("pagination")
		element=element.find_element_by_xpath(".//a[@class='confirm-nav next']")
		url=element.get_attribute('href')
        	element.click()
	except NoSuchElementException:
		break


